﻿function cartService(cart, shopStore) {
    this.cart = cart;
    this.shopStore = shopStore;

    this.addItem = function (description, price, quantity) {
        var totalInCart = this.cart.getTotalCount(description);
        var totalInShop = this.shopStore.getTotalCount(description);

        if (totalInCart + quantity <= totalInShop) {
            this.cart.addItem(description, price, quantity);
        } else {
            alert('Not enought products in shop!');
        }
    }

    this.buyNow = function () {
        if ((this.cart.products.length > 0) &&
            (this.shopStore.hasProductsInStore(this.cart.products))) {
            this.shopStore.removeProductsInStore(this.cart.products);
            this.cart.clearItems();
            return true;
        } else {
            return false;
        }
    }

    this.updateProductPrice = function (product) {
        for (var i = 0; i < this.cart.products.length; i++) {
            if (this.cart.products[i].description == product.description) {
                this.cart.products[i].price = product.price;
            }
        }
    }

}



