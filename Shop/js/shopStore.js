﻿shopStore.prototype = new store("shop3");
shopStore.prototype.constructor = shopStore;

function shopStore() {
    //this.addItem("Apple", 12, 90);
    //this.addItem("Avocado", 2, 3);

    this.addItem = function () {
        shopStore.prototype.addItem("New", 1, 1);
    },

    this.removeItem = function (index) {
        shopStore.prototype.products.splice(index, 1);
    },

    this.total = function () {
        var total = 0;
        angular.forEach(shopStore.prototype.products, function (item) {
            total += item.quantity * item.price;
        })

        return total;
    }

    this.hasProductsInStore = function (products) {
        for (var i = 0; i < products.length; i++) {
            var product = products[i];
            var totalInShop = shopStore.prototype.getTotalCount(product.description);
            if (product.quantity > totalInShop)
                return false;
        }
        return true;
    }
    this.removeProductsInStore = function(products) {
        for (var i = 0; i < products.length; i++) {
            var product = products[i];
            shopStore.prototype.addItem(product.description, product.price, product.quantity*-1);
        }
        shopStore.prototype.saveItems();
    }

}