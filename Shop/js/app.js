'use strict';


// App Module: the name AngularStore matches the ng-app attribute in the main <html> tag
// the route provides parses the URL and injects the appropriate partial page
var storeApp = angular.module('shop', ['ngRoute', 'ui.bootstrap']);

storeApp.config(function($routeProvider) {
    $routeProvider.when('/store', {
        templateUrl: 'partials/store.htm',
        controller: storeController
    });
    $routeProvider.when('/products/:productSku', {
        templateUrl: 'partials/product.htm',
        controller: storeController
    });
    $routeProvider.when('/cart', {
        templateUrl: 'partials/shoppingCart.htm',
        controller: storeController
    });
    $routeProvider.when('/adminPanel', {
        templateUrl: 'partials/adminPanel.htm',
        controller: adminController
    });
    $routeProvider.otherwise({
        redirectTo: '/store'
    });
});

// create a data service that provides a store and a shopping cart that
// will be shared by all views (instead of creating fresh ones for each view).
storeApp.factory("DataService", function () {

    // create store
    var myStore = new shopStore();

    // create shopping cart
    var myCart = new store("cartStore2");

    var myCartService = new cartService(myCart, myStore);
    return {
        store: myStore,
        cartService: myCartService,
        cart: myCart
    };
});

document.location.hash = '#/';

storeApp.directive('trackActive', function ($location) {
    function link(scope, element, attrs) {
        scope.$watch(function () {
            return $location.path();
        }, function () {
            var links = element.find('a');
            links.removeClass('active');
            angular.forEach(links, function (value) {
                var a = angular.element(value);
                if (a.attr('href') == '#' + $location.path()) {
                    a.addClass('active');
                }
            });
        });
    }
    return { link: link };
});