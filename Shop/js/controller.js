﻿'use strict';

// the storeController contains two objects:
// - store: contains the product list
// - cart: the shopping cart object
function storeController($scope, DataService, $modal, $compile) {

    // get store and cart from service
    $scope.store = DataService.store;
    $scope.cart = DataService.cart;
    $scope.cartService = DataService.cartService;




    $scope.open = function () {
        if (DataService.cartService.buyNow()) {
            $scope.dynamicPopover = "Thanks for shopping.";
            $scope.dynamicPopoverTitle = 'Products have been bought!';
        } else {
            $scope.dynamicPopover = null;
            $scope.dynamicPopoverTitle = null;
        }

        //var modalInstance = $modal.open({
        //    templateUrl: 'partials/buyNow.htm',
        //    controller: buyNowController,
        //    backdrop: true,
        //    keyboard: false
        //});

        //modalInstance.result.then(function (selectedItem) {
        //}, function () {});
    };

}

// the storeController contains two objects:
// - store: contains the product list
// - cart: the shopping cart object
function adminController($scope, $routeParams, DataService) {
    // get store and cart from service
    $scope.store = DataService.store;
    $scope.change = function (product) {
        DataService.cartService.updateProductPrice(product);
        $scope.store.saveItems();
    }
}

function buyNowController($scope, $modalInstance, DataService) {
    $scope.store = DataService.store;
    $scope.cart = DataService.cart;

    $scope.ok = function () {
        DataService.cartService.buyNow();

        $modalInstance.close();
    };

}