﻿function store(storeName) {
    this.storeName = storeName;
    this.clearStore = false;
    this.products = [];

    // load items from local storage when initializing
    this.loadItems();

    // save items to local storage when unloading
    var self = this;
    $(window).unload(function () {
        if (self.clearStore) {
            self.clearItems();
        }
        self.saveItems();
        self.clearStore = false;
    });
}

// load items from local storage
store.prototype.loadItems = function () {
    var products = localStorage != null ? localStorage[this.storeName + "_items"] : null;
    if (products != null && JSON != null) {
        try {
            var products = JSON.parse(products);
            for (var i = 0; i < products.length; i++) {
                var product = products[i];
                if (product.description != null && product.price != null && product.quantity != null) {
                    item = new cartItem(product.description, product.price, product.quantity);
                    this.products.push(product);
                }
            }
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}

// save items to local storage
store.prototype.saveItems = function () {
    if (localStorage != null && JSON != null) {
        localStorage[this.storeName + "_items"] = JSON.stringify(this.products);
    }
}

// adds an item to the cart
store.prototype.addItem = function (description, price, quantity) {
    quantity = this.toNumber(quantity);
    if (quantity != 0) {

        // update quantity for existing item
        var found = false;
        for (var i = 0; i < this.products.length && !found; i++) {
            var product = this.products[i];
            if (product.description == description && product.price == price) {
                found = true;
                product.quantity = this.toNumber(product.quantity + quantity);
                if (product.quantity <= 0) {
                    this.products.splice(i, 1);
                }
            }
        }

        // new item, add now
        if (!found && quantity > 0) {
            var product = new cartItem(description, price, quantity);
            this.products.push(product);
        }

        // save changes
        this.saveItems();
    }
}

// get the total price for all items currently in the cart
store.prototype.getTotalPrice = function (description) {
    var total = 0;
    for (var i = 0; i < this.products.length; i++) {
        var product = this.products[i];
        if (description == null || product.description == description) {
            total += this.toNumber(product.quantity * product.price);
        }
    }
    return total;
}

// get the total price for all items currently in the cart
store.prototype.getTotalCount = function (description) {
    var count = 0;
    for (var i = 0; i < this.products.length; i++) {
        var product = this.products[i];
        if (description == null || product.description == description) {
            count += this.toNumber(product.quantity);
        }
    }
    return count;
}

// clear the cart
store.prototype.clearItems = function () {
    this.products = [];
    this.saveItems();
}

store.prototype.toNumber = function (value) {
    value = value * 1;
    return isNaN(value) ? 0 : value;
}

//----------------------------------------------------------------
// items in the cart
//
function cartItem(description, price, quantity) {
    this.description = description;
    this.price = price * 1;
    this.quantity = quantity * 1;
}

